## Pre-implementation 

* Deploy Supporting Addon for PowerShell [Splunk\_SA\_powershell](https://splunkbase.splunk.com/app/1477/) by staging the app in deployment-apps
* Complete deployment for Windows Base [PT-OS-001-Microsoft-Windows](https://bitbucket.org/SPLServices/splunk_ta_windows)

## Data Acquisition Procedure Microsoft 2008R2+
Data collection for operational use cases including Windows Infra Structure App and general active directory functional monitoring

* Deployment Servers
	* Stage the following apps to apps
		* ``SecKit_all_deploymentserver_3_app_ms_dns``
	* Stage the following apps to deployment-apps
		* ``Splunk_TA_microsoft_dns``
		* ``Splunk_TA_microsoft_dns_SecKit_0_all_inputs``
	* Reload Deployment Server

##Post Implementation

Post Implementation continue to the following

* PT002-Splunk-Stream-DHCP (Links to be provided future)
* PT002-Splunk-Stream-DNS (Links to be provided future)